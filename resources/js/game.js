/*
    @authors: {
        frens: [
            @programista4k ,
            @JamnikWallenrod ,
            @grubypajonk ,
            @mniejniz0 ,
            @doee ,
            @tomash-pl ,
            @vizori ,
            @tomhet ,
            @patrolez
        ]
    }
*/

// ============================================================================
// model ======================================================================
// ============================================================================

// frontend -------------------------------------------------------------------
const battleProgressBarIterations = 10

const start_btn = document.getElementById("start-game");
const score_span = document.getElementById('user-score');
const status_span = document.getElementById('status');
const result_span = document.getElementById('resultSpan');
const result_div = document.getElementById('result');
const battleTime_div = document.getElementById('battleTime');
const battleTimeUsed_div = document.getElementById('battleTimeUsed');
const paper_div = document.getElementById("paper");
const hammer_div = document.getElementById("hammer");
const gun_div = document.getElementById("gun");
const chad_div = document.getElementById("chad");
const honkhonk_div = document.getElementById("honkhonk");
const polka_div = document.getElementById("polka");

const startBattleTimeWidth = battleTime_div.getBoundingClientRect().width
var displayBattleResultMs = 500

// backend --------------------------------------------------------------------
const frens = {
    PAPER:    { type: 'PAPER', div: paper_div, power: 1 },
    HAMMER:   { type: 'HAMMER', div: hammer_div, power: 2 },
    GUN:      { type: 'GUN', div: gun_div, power: 3 }
}

const frensArr = Object.values(frens)

const enemies = {
    POLKA:    { type: 'POLKA', div: polka_div, power: -1 },
    HONKHONK: { type: 'HONKHONK', div: honkhonk_div, power: -2 },
    CHAD:     { type: 'CHAD', div: chad_div, power: -3 }
}

const enemiesArr = Object.values(enemies)

const status = {
    READY:    { type: 'READY', msg: 'Gotowy?', msg2: '', color: 'grey' },
    FIGHT:    { type: 'FIGHT', msg: 'Walka!', msg2: '', color: 'black' },
    LOST:     { type: 'LOST', msg: '#przegryw', msg2: 'Przegranko', color: 'red' },
    VICTORY:  { type: 'VICTORY', msg: 'Wygranko', msg2: 'Wygranko', color: 'blue' }
}

var currentEnemy
var currentFren
var currentStatus = status.IDLE
var currentScore = 0
var startMsBeforeBattle = 5000
var currentMsBeforeBattle = 5000
var difficultyRate = 0.1


// ============================================================================
// logic ======================================================================
// ============================================================================

// frontend--------------------------------------------------------------------
paper_div.addEventListener('click', function() {
    if (currentStatus === status.READY) {
        setCurrentFren(frens.PAPER)
        setCurrentStatus(status.FIGHT)
    }
})

hammer_div.addEventListener('click', function() {
    if (currentStatus === status.READY) {
        setCurrentFren(frens.HAMMER)
        setCurrentStatus(status.FIGHT)
    }
})

gun_div.addEventListener('click', function() {
    if (currentStatus === status.READY) {
        setCurrentFren(frens.GUN)
        setCurrentStatus(status.FIGHT)
    }
})

function hoverDiv(div) {
    div.style.background = "red";
}

function unHoverDiv(div) {
    div.style.background = "transparent"
}

function unHoverAllFrens() {
    frensArr.forEach(function(fren) {
        unHoverDiv(fren.div);
    });
}

function unHoverAllEnemies() {
    enemiesArr.forEach(function(enemy) {
        unHoverDiv(enemy.div)
    })
}

function unHoverAll() {
    unHoverAllFrens()
    unHoverAllEnemies()
}

function displayBattleResult(result) {
    result_span.style.color = result.color
    result_span.innerHTML = result.msg2
    result_div.classList.add("show")
    setTimeout(() => {
        result_div.classList.remove('show')
    }, displayBattleResultMs);
}

function updateFrontStatus() {
    status_span.innerHTML = currentStatus.msg
}

function updateFrontScore() {
    score_span.innerHTML = currentScore
}

function moveBattleProgressBar(ms) {
    battleTime_div.classList.add('show')
    battleTimeUsed_div.style.width = `${startBattleTimeWidth}px`

    const fullWidth = battleTime_div.getBoundingClientRect().width
    const step = fullWidth / battleProgressBarIterations

    var interval
    function progress() {
        const currentWidth = battleTimeUsed_div.getBoundingClientRect().width
        if (currentWidth > 0) {
            battleTimeUsed_div.style.width = `${(currentWidth - step)}px`
        } else {
            clearInterval(interval)
        }
    }
    interval = setInterval(progress, ms / battleProgressBarIterations)
    progress()
}


// backend --------------------------------------------------------------------

async function game() {
    onStart()
    while(currentStatus != status.LOST) {
        await battle()
        unHoverAll()
    }
    onEnd()
}

function onStart() {
    unHoverAll()
    start_btn.style.visibility = "hidden"
    setCurrentScore(0)
    this.currentMsBeforeBattle = this.startMsBeforeBattle
    setCurrentStatus(status.READY)
}

function onEnd() {
    start_btn.style.visibility = "visible";
    battleTime_div.classList.remove('show')
    unHoverAll()
}

async function battle() {
    currentFren = null
    currentMsBeforeBattle = currentMsBeforeBattle - currentMsBeforeBattle * difficultyRate

    setCurrentStatus(status.READY)

    setCurrentEnemy(randomEnemy())
    console.log("currentEnemy: ", currentEnemy)


    console.log("You got " + currentMsBeforeBattle + " millis to choose a fren.")
    await moveBattleProgressBar(currentMsBeforeBattle)
    await sleep(currentMsBeforeBattle)
    console.log("currentFren: ", currentFren)

    fight()
}

const fight = () => {
    if (isFrenWin(currentFren, currentEnemy)) {
        won()
    } else {
        lost()
    }
    displayBattleResult(currentStatus)
}

function isFrenWin(fren, enemy) {
    return fren != null && fren.power + enemy.power == 0;
}

function won() {
    console.log("Victory!")
    setCurrentScore(++currentScore)
    setCurrentStatus(status.VICTORY)
}

function lost() {
    console.log("Lost.")
    setCurrentStatus(status.LOST)
}

function randomEnemy() {
    const enemySize = Object.keys(enemiesArr).length;
    const id = Math.floor(Math.random() * enemySize);
    return enemiesArr[id];
}

function setCurrentFren(fren)  {
    unHoverAllFrens()
    hoverDiv(fren.div)
    this.currentFren = fren
}

function setCurrentEnemy(enemy)  {
    unHoverAllEnemies()
    hoverDiv(enemy.div)
    this.currentEnemy = enemy
}

function setCurrentStatus(status) {
    this.currentStatus = status
    updateFrontStatus()
}

function setCurrentScore(score) {
    this.currentScore = score
    score_span.innerHTML = score
    updateFrontScore()
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}